//task 1
let x = 30
let y = 20
if (x < y){
    console.log(x + y)
}else if(x > y){
    console.log(x - y)
}else{
    console.log(x * y)
}
//task 2
let a = [1, 2, 3, -5, 0, 10]
let b =  [5, -1, 7]

function addArrays(a,b){
    let sumArrayA = 0
    let sumArrayB = 0
    for (let i = 0; i < a.length; i++){
        sumArrayA +=a[i]


    }
    for(let i = 0; i < b.length; i++){

        sumArrayB +=b[i]
    }
    return sumArrayA + sumArrayB

}
let result = addArrays(a,b)
console.log(result)
//task 3
let myArr=[true, false, false, true, false]

function countTrue(a){
    let sumTrue = 0
    for(let i in myArr){
       sumTrue +=myArr[i]
    }
    return sumTrue

}
console.log(countTrue(myArr))
//task 4
let n= 14

function doubleFactorial(n){
    let multi=1
    if(n % 2 == 0){
        for(let i=2; i<=n;i+=2){
            multi *=i
        }
        return multi

    }else{
        for(let i=1;i<=n;i+=2){
            multi *=i
        }
    }return multi


}
console.log(doubleFactorial(n))
//task 5.1
const Person = [
    {name: "Nata", age:19},
    {name:"Olha",age: 19}
]
let age1 = Person[0]["age"]
let age2 = Person[1]['age']
let name1 = Person[0]["name"]
let name2 = Person[1]['name']

function compareAge(Person){

    if(age1 > age2){
        return name1 +" is older than " + name2


    }else {
        if (age1 < age2) {
            return name1 + " is younger than " + name2


        } 

            return name1 +" is the same age as " + name2

        
    }

}
console.log(compareAge(Person))
//task 5.2
const  Person = [
    {name:"Zinaida",age: 15},
    {name: "Varvara",age: 20},
    {name: "Olha", age: 25},
    {name: "Taras", age: 40},
    {name: "Victoria", age: 33}
]
//in ascending order
Person.sort(mySortAsc)
function mySortAsc(a,b){
    if(a.age>b.age)
        return 1
    if(a.age<b.age)
        return -1
    return 0
}
Person.sort(mySortAsc)
console.log(Person)

//in descending order

function mySortDesc(a,b){
    if(a.age<b.age)
        return 1
    if(a.age>b.age)
        return -1
    return 0
}
Person.sort(mySortDesc)
console.log(Person)